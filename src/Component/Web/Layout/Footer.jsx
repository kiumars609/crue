import React from "react";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <>
      <footer className="col-md-12 footer">
        <div className="col-md-1 mx-5 footer-logo">
          <img
            src="/assets/images/logo/logo.png"
            className="col-md-12"
            alt="logo"
          />
        </div>
        <div className="col-md-4 footer-directions">
          <Link className="direct">About</Link>
          <Link className="direct">Carrer</Link>
          <Link className="direct">Benefits</Link>
          <Link className="direct">Support</Link>
          <p>&copy; 2023 CREU. All right reserved.</p>
        </div>
        <div className="col-md-5 footer-socials">
          <div className="col-md-12 d-flex justify-content-end">
            <Link className="social">
              <i className="fa fa-facebook-f"></i>
            </Link>
            <Link className="social">
              <i className="fa fa-twitter"></i>
            </Link>
            <Link className="social">
              <i className="fa fa-twitch"></i>
            </Link>
            <Link className="social">
              <i className="fa fa-youtube-play"></i>
            </Link>
            <Link className="social">
              <i className="fa fa-instagram"></i>
            </Link>
          </div>
          <p>Support: crue@info.com</p>
        </div>
      </footer>
    </>
  );
}
