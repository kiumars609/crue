import React from "react";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <>
      <nav className="col-md-10 mx-auto mt-5 navabr-stylish">
        <Link to={"/"} className="link">
          Home
        </Link>
        <a href="#" className="link">
          ShowRoom
        </a>
        <Link to={"/"} className="logo col-md-3 p-0">
          <img src="/assets/images/logo/logo.png" className="col-md-12" />
        </Link>
        <a href="#" className="link">
          About Us
        </a>
        <a href="#" className="link">
          Find Us
        </a>
      </nav>
    </>
  );
}
