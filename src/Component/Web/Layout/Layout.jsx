import React, { useEffect } from "react";
import "./scss/Layout.css";
import Navbar from "./Navbar";
import Footer from "./Footer";
import Modal from "../../../Pages/CustomPage/Modal";

export default function Layout({ pageTitle, children }) {
  useEffect(() => {
    document.title = "CRUE | " + pageTitle;
  }, [pageTitle]);

  return (
    <>
      <Navbar />
      {children}
      <Footer />
    </>
  );
}
