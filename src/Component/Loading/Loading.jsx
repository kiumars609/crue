import React from 'react'
// import address from './loading.svg'
import address from './loader2.gif'
import './style.css'

export default function Loading() {
    return (
        <div className='main-loading col-12 h-100'>
            <img src={address} className='col-md-8' alt='' />
        </div>
    )
}
