import React from "react";
import "./scss/landingPage.css";
import { Link } from "react-router-dom";

export default function LandingPage() {
  return (
    <>
      <div className="col-md-10 mx-auto">
        <Link to={"/custom"}>
          <span className="c-style">
            C<span className="descrip">Custom</span>
          </span>
        </Link>
        <span className="r-style">
          R<span className="descrip">Repairing</span>
        </span>
        <span className="e-style">
          E<span className="descrip">Embellish</span>
        </span>
        <span className="u-style">
          U<span className="descrip">Upgrading</span>
        </span>
      </div>
    </>
  );
}
