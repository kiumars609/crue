import React from "react";

export default function PanelOption({
  className,
  title,
  data,
  handleCheckout,
}) {
  const query =
    data &&
    data.map((item, index) => {
      return (
        <div className="box col-md-6" key={index}>
          <div className="box-icon" onClick={(e) => handleCheckout(item.name,item.price)}>
            <img
              src={`/assets/images/icon/${item.icon}`}
              className="col-md-12"
            />
          </div>
          <p className="box-text">{item.name}</p>
        </div>
      );
    });

  return (
    <>
      <div className={`col-md-12 panel-product-left-${className}`}>
        <span className="title">{title} </span>
        <div className="col-md-12 d-flex flex-wrap justify-content-end p-0 mt-4">
          {query}
        </div>
      </div>
    </>
  );
}
