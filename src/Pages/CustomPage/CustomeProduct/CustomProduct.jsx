import React, { useState, useEffect } from "react";
import Layout from "../../../Component/Web/Layout";
import "./scss/CustomProduct.css";
import { useNavigate, useParams } from "react-router-dom";
import Data from "./Data";
import InfoModal from "./InfoModal";
import ColorButton from "./ColorButton";
import PanelOption from "./PanelOption";

export default function CustomProduct() {
  const { name } = useParams();
  const productTitle = name;
  const [color, setColor] = useState([]);
  const [colorPrice, setColorPrice] = useState(0);
  const [hardPrice, setHardPrice] = useState(0);
  const [total, setTotal] = useState([]);
  const [activeCheckout, setActiveCheckout] = useState(false);
  const { data } = Data();
  const ps5Data = data[0];

  const navigate = useNavigate();

  useEffect(() => {
    setColor(ps5Data["colorDefault"][0]);
  }, []);

  const handleChangeColor = (color) => {
    setColor(ps5Data[`color_${color}`][0]);
    setColorPrice(ps5Data[`color_${color}`][0].price);
  };

  const hddData = [
    { name: "500G SSD", price: 1000000, icon: "ssd.png" },
    { name: "1T SSD", price: 1200000, icon: "ssd.png" },
  ];

  const handleCheckout = (productName, productPrice) => {
    setActiveCheckout(true);
    setHardPrice([productName, productPrice]);
  };

  useEffect(() => {
    setTotal([colorPrice, hardPrice[1]]);
  }, [colorPrice, hardPrice[1]]);

  const handleSubmit = (e) => {
    e.preventDefault();
    navigate("/checkout", {
      state: [
        {
          productName: ps5Data["faName"],
          productColor: color.colorName,
          productHard: hardPrice[0],
          totalPrice: total[0] + total[1],
        },
      ],
    });
  };

  return (
    <>
      <Layout pageTitle={"Custom"}>
        <div className="col-md-11 mx-auto panel-product">
          {/* Start Left Side */}
          <div className="col-md-3 panel-product-left">
            <div
              className="col-md-12 panel-product-left-tecnicalInfo"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              <span className="title">اطلاعات فنی </span>
              <i class="fa fa-list" aria-hidden="true"></i>
            </div>
            <InfoModal
              data={ps5Data["specifications"][0]}
              title={ps5Data["faName"]}
            />

            <div className="col-md-12 panel-product-left-panel-color">
              <span className="title">رنگ بدنه </span>
              <div className="col-md-12 d-flex flex-wrap justify-content-end p-0 mt-4">
                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"pink"}
                  text={"نوا صورتی"}
                  colorCode={"#E73D99"}
                />

                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"purple"}
                  text={"بنفش کهکشانی"}
                  colorCode={"#5739AF"}
                />

                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"blue"}
                  text={"نور ستاره آبی"}
                  colorCode={"#5EB9F1"}
                />

                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"red"}
                  text={"قرمز کیهانی"}
                  colorCode={"#9E1C4C"}
                />

                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"black"}
                  text={"سیاه نیمه شب"}
                  colorCode={"#202226"}
                />

                <ColorButton
                  handleChangeColor={handleChangeColor}
                  colorName={"gray"}
                  text={"استتار خاکستری"}
                  colorCode={"#858286"}
                />
              </div>
            </div>

            {/* Start Hard Disk Panel */}
            <PanelOption
              className={"storage-panel"}
              title={"هارد دیسک"}
              data={hddData}
              handleCheckout={handleCheckout}
            />
            {/* End Hard Disk Panel */}
          </div>
          {/* End Left Side */}

          {/* Start Right Side */}
          <div className="col-md-9 panel-product-right">
            <div className="col-md-12">
              <h2 className="panel-product-header">
                {productTitle + " "} |{" "}
                <span className="fa-type">{[ps5Data["faName"]]}</span>
              </h2>
            </div>

            <div className="col-md-12 d-flex justify-content-end">
              <div
                id="carouselExampleIndicators"
                className="carousel carousel-dark slide col-md-11"
                data-bs-ride="carousel"
              >
                <div className="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    className="active"
                    aria-current="true"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                </div>
                <div className="carousel-inner col-md-12">
                  <div className="carousel-item active text-center">
                    <img
                      src={`/assets/images/products/${color["image1"]}`}
                      className="col-md-10"
                      alt="..."
                    />
                  </div>
                  <div className="carousel-item text-center">
                    <img
                      src={`/assets/images/products/${color["image2"]}`}
                      className="col-md-10"
                      alt="..."
                    />
                  </div>
                  <div className="carousel-item text-center">
                    <img
                      src={`/assets/images/products/${color["image3"]}`}
                      className="col-md-10"
                      alt="..."
                    />
                  </div>
                </div>
                <button
                  className="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Previous</span>
                </button>
                <button
                  className="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="visually-hidden">Next</span>
                </button>
              </div>
            </div>
          </div>
          {/* End Right Side */}

          {/* Start Other Information */}

          <div className="col-md-12 panel-product-information">
            <hr className="col-md-11 mx-auto" />
            <div className="col-md-12 panel-product-information-intro">
              <h4>معرفی</h4>
              <img
                src={`/assets/images/products/${ps5Data.productType}/${ps5Data.brand}/${ps5Data.enName}/${ps5Data.mainImage}`}
                alt={ps5Data.faName}
                className="col-md-6 mx-auto mb-5 d-flex"
              />
              <p>{ps5Data.technicalInfo}</p>
            </div>
          </div>
          {/* End Other Information */}
        </div>

        {/* Start Total Selected Product */}
        <div
          className={`col-md-12 checkout-panel ${activeCheckout &&
            "checkout-animation"}`}
        >
          {[ps5Data["faName"]] + " + " + color.colorName + " + " + hardPrice[0]}
          <p>Total: {total[0] + total[1]}</p>

          <form>{/* <input type="hidden"  /> */}</form>

          <button className="btn btn-danger" onClick={(e) => handleSubmit(e)}>
            ثبت سفارش
          </button>
        </div>
        {/* End Total Selected Product */}
      </Layout>
    </>
  );
}
