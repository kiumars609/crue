import React from "react";

export default function Data() {
  const data = [
    {
      productType: "console",
      name: "PlayStation 5",
      faName: "پلی استیشن 5",
      enName: "ps5",
      brand: "sony",
      mainImage:"main.png",
      technicalInfo:
        "کنسول بازی سونی مدل Playstation 5 ظرفیت 825 گیگابایت یکی از محصولات بسیار حرفه‌ای و خواستنی در رده کنسول‌هلای بازی در دنیا است که توسط کمپانی سونی ارائه شده است. این کنسول با اعمال تغییراتی ظاهری و سخت‌افزاری طراحی شده است. درایو بازی این محصول همچنان Ultra HD Blu-ray, up to 100GB/disc است و خروجی تصویر آن، از فرمت 4K پشتیبانی می‌کند. پردازشگر اصلی و گرافیکی این مدل تغییر اساسی کرده و از سری 4 قدرت بالاتری دارد. همچنین پلی‌استیشن 5 به‌واسطه پورت LAN یا اتصال بی‌سیم Wi-Fi به اینترنت متصل می‌شود و امکانات متعددی را برای کاربران فراهم می‌کند که بتوانند با عضویت در شبکه‌ی PSN و پرداخت هزینه، بازی‌های مورد نظر خود را خریداری و دانلود کنند یا اینکه به صورت آنلاین با دوستان خود بازی کنند. پورت‌ USB از نوع Type-C به آن اضافه شده است. دسته‌ها از طریق بلوتوث به کنسول متصل می‌شوند و برد مورد نیاز را پشتیبانی می‌کند. خروجی تصویر، پورت HDMI است و کیفیت 4K را پشتیبانی می‌کند. این مدل از حافظه‌ی داخلی 825GB بهره‌مند است.",
      specifications: [
        {
          dimensions: "۲۶۰x۱۰۴x۳۹۰ سانتی‌متر",
          types_of_memory: "هارد دیسک",
          hard_disk_capacity: "۸۲۵ گیگابایت",
          appearance_features: "دسته بی سیم",
          number_of_categories: "یک عدد",
          communication_technologies: "بلوتوث",
          features_and_capabilities:
            "- Ethernet (۱۰BASE-T, ۱۰۰BASE-TX, ۱۰۰۰BASE-T) - IEEE ۸۰۲.۱۱ a/b/g/n/ac/ax",
          sound_output: "HDMI",
          image_output: "HDMI با پشتیبانی از فرمت‌های ۴K ۱۲۰Hz TVs ،HDR",
          ram: "۱۶GB GDDR۶/۲۵۶-bit",
          gpu: "Ray Tracing Acceleration Up to ۲.۲۳ GHz (۱۰.۳ TFLOPS)",
          cpu: "AMD Ryzen Zen ۸ Cores",
          console_drive_type: "Ultra HD Blu-ray, up to ۱۰۰GB/disc",
          weight: "۴۵۰۰ گرم",
        },
      ],
      colorDefault: [
        {
          colorName: "black",
          image1: "console/sony/ps5/black-1.webp",
          image2: "console/sony/ps5/black-2.webp",
          image3: "console/sony/ps5/black-3.webp",
        },
      ],

      color_purple: [
        {
          colorName: "purple",
          image1: "console/sony/ps5/purple-1.webp",
          image2: "console/sony/ps5/purple-2.webp",
          image3: "console/sony/ps5/purple-3.webp",
          price: 110000,
        },
      ],
      color_black: [
        {
          colorName: "black",
          image1: "console/sony/ps5/black-1.webp",
          image2: "console/sony/ps5/black-2.webp",
          image3: "console/sony/ps5/black-3.webp",
          price: 160000,
        },
      ],
      color_red: [
        {
          colorName: "red",
          image1: "console/sony/ps5/red-1.webp",
          image2: "console/sony/ps5/red-2.webp",
          image3: "console/sony/ps5/red-3.webp",
          price: 140000,
        },
      ],
      color_pink: [
        {
          colorName: "pink",
          image1: "console/sony/ps5/pink-1.webp",
          image2: "console/sony/ps5/pink-2.webp",
          image3: "console/sony/ps5/pink-3.webp",
          price: 120000,
        },
      ],
      color_blue: [
        {
          colorName: "blue",
          image1: "console/sony/ps5/blue-1.webp",
          image2: "console/sony/ps5/blue-2.webp",
          image3: "console/sony/ps5/blue-3.webp",
          price: 115000,
        },
      ],
      color_gray: [
        {
          colorName: "gray",
          image1: "console/sony/ps5/gray-1.webp",
          image2: "console/sony/ps5/gray-2.webp",
          image3: "console/sony/ps5/gray-3.webp",
          price: 100000,
        },
      ],
    },
  ];

  return { data };
}
