import React from "react";

export default function ColorButton({
  handleChangeColor,
  colorName,
  text,
  colorCode,
}) {
  return (
    <>
      <div className="color-button col-md-6">
        <button
          className={`btn col-md-12`}
          style={{ backgroundColor: colorCode, borderColor: colorCode === '#202226' ? '#eeeeee' : '' }}
          onClick={(e) => handleChangeColor(colorName)}
        ></button>
        <span className="color-button-text">{text}</span>
      </div>
    </>
  );
}
