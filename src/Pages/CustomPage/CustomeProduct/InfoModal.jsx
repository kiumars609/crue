import React from "react";

export default function InfoModal({ data, title }) {
  return (
    <>
      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
        dir="rtl"
      >
        <div className="modal-dialog modal-dialog-scrollable modal-xl">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                <span> اطلاعات فنی </span>
                <span> {title}</span>
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="col-md-12 info-modal-data">
                <span>ابعاد</span>
                <span>{data.dimensions}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>انواع حافظه</span>
                <span>{data.types_of_memory}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>ظرفیت هارد دیسک</span>
                <span>{data.hard_disk_capacity}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>امکانات ظاهری</span>
                <span>{data.appearance_features}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>تعداد دسته</span>
                <span>{data.number_of_categories}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>فناوری‌های ارتباطی</span>
                <span>{data.communication_technologies}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>امکانات و قابلیت‌ها</span>
                <span>{data.features_and_capabilities}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>خروجی صدا</span>
                <span>{data.sound_output}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>خروجی تصویر</span>
                <span>{data.image_output}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>حافظه RAM</span>
                <span>{data.ram}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>پردازشگر گرافیکی GPU</span>
                <span>{data.gpu}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>پردازشگر اصلی CPU</span>
                <span>{data.cpu}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>نوع درایو کنسول</span>
                <span>{data.console_drive_type}</span>
              </div>
              <div className="col-md-12 info-modal-data">
                <span>وزن</span>
                <span>{data.weight}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
