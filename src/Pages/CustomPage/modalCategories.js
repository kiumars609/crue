export function modalCategories(value) {
  let data = "";
  if (value === "Console") {
    data = [
      {
        title: "Sony",
        category: value,
        detail: [{ name: "PlayStation 4" }, { name: "PlayStation 5" }],
      },
      {
        title: "Microsoft",
        category: value,
        detail: [{ name: "Xbox Series X" }, { name: "Xbox Series S" }],
      },
      {
        title: "Nintendo",
        category: value,
        detail: [{ name: "Nintendo Switch" }, { name: "WII U" }],
      },
    ];
  }

  if (value === "House") {
    data = [
      {
        title: value,
        category: value,
        detail: [{ name: "Living Room" }, { name: "Bedroom" }],
      },
    ];
  }

  if (value === "PC") {
    data = [
      {
        title: "PC",
        category: value,
        detail: [{ name: "All in One" }, { name: "Case" }],
      },
    ];
  }
  return { data };
}
