import React, { useState } from "react";
import { modalCategories } from "./modalCategories";
import { Data } from "./Data";

export default function Modal({ show, hide, value }) {
  const { data } = modalCategories(value);
  const { queryTitle, queryDetails, clearData } = Data(data);

  return (
    <>
      {/* <!-- Start Navbar Section --> */}
      <nav className="navbar">
        <input
          type="checkbox"
          id="check"
          className="checkbox"
          checked={show && "checked"}
          hidden
        />
        <div className="navbar-navigation">
          {/* Left Side of Modal */}
          <div className="navbar-navigation-left">
            <ul className="nav-list">
              {queryTitle}
              <li className="nav-list-item">
                <span
                  href="#"
                  className="nav-list-link"
                  onClick={(e) => {
                    hide();
                    clearData(true);
                  }}
                >
                  Exit
                </span>
              </li>
            </ul>
          </div>
          {/* Right Side of Modal */}
          <div className="navbar-navigation-right">
            <div className="col-md-12 modal-data-results">{queryDetails}</div>
          </div>
        </div>
      </nav>
    </>
  );
}
