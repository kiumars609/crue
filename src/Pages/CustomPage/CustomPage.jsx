import React, { useState } from "react";
import "./scss/CustomPage.css";
import { Link } from "react-router-dom";
import Layout from "../../Component/Web/Layout/Layout";
import Modal from "./Modal";

export default function CustomPage() {
  const [show, setShow] = useState(false);
  const [value, setValue] = useState([]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const categories = [
    { category: "Console", image: "/assets/images/icon/controller.webp" },
    { category: "House", image: "/assets/images/icon/room.webp" },
    { category: "PC", image: "/assets/images/icon/pcCase.webp" },
    { category: "Laptop", image: "/assets/images/icon/laptop.webp" },
    { category: "Phone", image: "/assets/images/icon/phone.webp" },
    { category: "Tablet", image: "/assets/images/icon/tablet.webp" },
  ];

  const handleModal = (item) => {
    if (item) {
      handleShow();
      setValue(item);
    }
  };

  const queryCategories =
    categories &&
    categories.map((item, index) => {
      return (
        <div
          className="card-category"
          key={index}
          onClick={(e) => handleModal(item.category)}
        >
          <img src={item.image} className="col-md-12" />
          <p className="card-tt">test</p>
          <div className="col-md-6 card-left">
            <span>Customise</span>
          </div>
          <div className="col-md-6 card-right">
            <span>{item.category}</span>
          </div>
        </div>
      );
    });

  return (
    <>
      <Layout pageTitle={"Custom"}>
        <div className="col-md-8 mx-auto main-types">{queryCategories}</div>

        <Modal show={show} hide={handleClose} value={value} />
      </Layout>
    </>
  );
}
