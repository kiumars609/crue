import React, { useState } from "react";
import { Link } from "react-router-dom";

export function Data(data) {
  const [getDetail, setGetDetail] = useState([]);

  // *********** Set datails in state ***********
  const handleGetDetail = (title, value) => {
    // Console Data
    if (title === "Sony") {
      setGetDetail(value);
    }
    if (title === "Microsoft") {
      setGetDetail(value);
    }
    if (title === "Nintendo") {
      setGetDetail(value);
    }
    // House Data
    if (title === "House") {
      setGetDetail(value);
    }
    // PC Data
    if (title === "PC") {
      setGetDetail(value);
    }
  };

  // *********** Get Title of Categories ***********
  const queryTitle =
    data &&
    data.map((item, index) => {
      // Console Data
      if (item.category === "Console") {
        return (
          <li className="nav-list-item" key={index}>
            <a
              href="#"
              className="nav-list-link"
              onClick={(e) => handleGetDetail(item.title, item.detail)}
            >
              {item.title}
            </a>
          </li>
        );
      }

      // House Data
      if (item.category === "House") {
        return (
          <li className="nav-list-item" key={index}>
            <a
              href="#"
              className="nav-list-link"
              onClick={(e) => handleGetDetail(item.title, item.detail)}
            >
              {item.title}
            </a>
          </li>
        );
      }

      // PC Data
      if (item.category === "PC") {
        return (
          <li className="nav-list-item" key={index}>
            <a
              href="#"
              className="nav-list-link"
              onClick={(e) => handleGetDetail(item.title, item.detail)}
            >
              {item.title}
            </a>
          </li>
        );
      }
    });

  // *********** Get All Details of Categories ***********
  const queryDetails =
    getDetail &&
    getDetail.map((item, index) => {
      return (
        <Link to={`/custom-product/${item.name}`} key={index} className="data-results">
        {item.name}
        </Link>
      );
    });

  // *********** Clear modal data after close modal ***********
  const clearData = (value) => {
    if (value) {
      setGetDetail([]);
    }
  };

  return { queryTitle, queryDetails, clearData };
}
