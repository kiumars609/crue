import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

export default function Error404() {
  return (
    <>
      <div className="error-area">
        <img
          src="/assets/images/404.jpg"
          alt="error404"
          className="col-md-100"
        />
        <div className="button">
          <Link to="/" className="btn">
            <span>Back to Home</span>
          </Link>
        </div>
      </div>
    </>
  );
}
