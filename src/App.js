import React from "react";
import { Route, Routes } from "react-router-dom";
import { useScrollToTop } from "./CustomHooks/useScrollToTop";
import LandingPage from "./Pages/LandingPage/";
import Error404 from "./Pages/Error404/";
import CustomPage from "./Pages/CustomPage/CustomPage";
import CustomProduct from "./Pages/CustomPage/CustomeProduct/CustomProduct";
import Checkout from "./Pages/CustomPage/Checkout/Checkout";

function App() {
  useScrollToTop();
  return (
    <>
      <Routes>
        <Route path="/" exact element={<LandingPage />} />
        <Route path="/custom" element={<CustomPage />} />
        <Route path="/custom-product/:name" element={<CustomProduct />} />
        <Route path="/checkout" element={<Checkout />} />
        {/* 404 Rout */}
        <Route path="/*" element={<Error404 />} />
      </Routes>
    </>
  );
}

export default App;
