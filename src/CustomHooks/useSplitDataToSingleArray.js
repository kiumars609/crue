import React from "react";
import { Link } from "react-router-dom";

export function useSplitDataToSingleArray(data, style, src) {
  const splited = data.split("/");
  const query = splited.map((i, index) => {
    return (
      <Link key={index} className={style} to={`/${src}/${i}`}>
        {i + "\u00A0"}
      </Link>
    );
  });
  let splitedQuery = null;
  const query2 = splited.map((i) => (splitedQuery = i));

  return { query, query2 };
}
